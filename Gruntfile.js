'use strict';
 
module.exports = function(grunt) {
 
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    connect: {
      server: {
        options: {
          port: 8000,
          base: './public'
        }
      }
    },
    watch: {
      test: {
        files: ['public/js/**/*.js', 'public/specs/**/*Spec.js'],
        tasks: ['connect', 'jasmine']
      },
    },
    jasmine: {
        test: {
            src: [
              'public/js/support.js',
              'public/js/committers_provider.js', 
              'public/js/committer.js', 
              'public/js/committers_list.js',
              'public/js/committers_view.js',
              'public/js/app.js'
            ],
            options: {
                vendor: [
                    'public/lib/jquery.js',
                    'public/lib/underscore/underscore.js',
                    'public/lib/backbone/backbone.js',
                    'public/lib/sinon/sinon.all.js',
                    'public/lib/jasmine-jquery/lib/jasmine-jquery.js',

                ],
                specs: 'public/specs/**/*Spec.js'
            }
        }
    }
  });
 
  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
 
  // Default task.
  grunt.registerTask('test', ['connect', 'jasmine']);
 
};