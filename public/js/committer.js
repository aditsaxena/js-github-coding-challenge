// Generated by CoffeeScript 1.7.1
(function() {
  window.Committer = Backbone.Model.extend({
    defaults: {
      user: "",
      name: "",
      gravatar: "",
      commits: 0
    }
  });

}).call(this);
