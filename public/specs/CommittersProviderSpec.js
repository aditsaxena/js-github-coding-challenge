// Generated by CoffeeScript 1.7.1
(function() {
  describe("CommittersProvider", function() {
    var commitsProvider;
    commitsProvider = void 0;
    beforeEach(function() {
      return commitsProvider = new CommittersProvider;
    });
    it(".importCommits on random html", function() {
      var html_page;
      html_page = "<body><div>random text</div></body>";
      commitsProvider.importCommits(html_page);
      return expect(commitsProvider.commits).toEqual([]);
    });
    it(".importCommits with valid data", function() {
      var html_page;
      html_page = ["<body><div>", "<p class=\"some-class commit\"><span><img alt=\"Pierre\" data-user=\"999\" src=\"http://site.com/pierre.png\"></span></p>", "<p class=\"some-class commit\"><span><img alt=\"Jane\" data-user=\"111\" src=\"http://site.com/jane.png\"></span></p>", "</div></body>"].join("");
      commitsProvider.importCommits(html_page);
      return expect(commitsProvider.commits).toEqual([
        {
          user: 999,
          name: 'Pierre',
          gravatar: 'http://site.com/pierre.png'
        }, {
          user: 111,
          name: 'Jane',
          gravatar: 'http://site.com/jane.png'
        }
      ]);
    });
    it(".convertCommitsToCommitters", function() {
      commitsProvider.commits = [
        {
          user: 111,
          name: 'Jane',
          gravatar: 'http://site.com/jane.png'
        }, {
          user: 999,
          name: 'Pierre',
          gravatar: 'http://site.com/pierre.png'
        }, {
          user: 111,
          name: 'Jane',
          gravatar: 'http://site.com/jane.png'
        }, {
          user: 777,
          name: 'Adrian',
          gravatar: 'http://site.com/adrian.png'
        }, {
          user: 111,
          name: 'Jane',
          gravatar: 'http://site.com/jane.png'
        }, {
          user: 999,
          name: 'Pierre',
          gravatar: 'http://site.com/pierre.png'
        }, {
          user: 111,
          name: 'Jane',
          gravatar: 'http://site.com/jane.png'
        }
      ];
      return expect(commitsProvider.convertCommitsToCommitters()).toEqual([
        {
          user: 111,
          name: 'Jane',
          gravatar: 'http://site.com/jane.png',
          commits: 4
        }, {
          user: 999,
          name: 'Pierre',
          gravatar: 'http://site.com/pierre.png',
          commits: 2
        }, {
          user: 777,
          name: 'Adrian',
          gravatar: 'http://site.com/adrian.png',
          commits: 1
        }
      ]);
    });
  });

}).call(this);
