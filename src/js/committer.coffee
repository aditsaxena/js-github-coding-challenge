# Committer model

window.Committer = Backbone.Model.extend
  defaults:
    user: ""
    name: ""
    gravatar: ""
    commits: 0

