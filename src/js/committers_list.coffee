# List of Committers

commitsProvider = new CommittersProvider

window.CommittersList = Backbone.Collection.extend
  
  url: "static/commits.html" # we can't use the online version due to same origin resources restrictions
  
  parse: (html_page) ->
    commitsProvider.provider html_page

  fetch: (options) ->
    options = options or {}
    options.dataType = "html"
    Backbone.Collection::fetch.call this, options

  model: Committer

