# Rendering View
window.CommittersView = Backbone.View.extend
  
  committers: new CommittersList()
  
  style_size: (commits) ->
    size = (commits - 1) * 10 + 50
    "width:" + size + "px; height:" + size + "px;"

  initialize: ->
    _.bindAll this, "render"

    return unless $("#content").length > 0

    @committerTpl = _.template $("#committer-tpl").html()

    @committers.fetch success: this.render

  render: ->
    the_html = ""
    self = this
    _.each @committers.models, (commit, i) ->
      the_html += self.committerTpl(commit.attributes)

    $('#content').html the_html
