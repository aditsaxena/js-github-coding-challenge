# Convert HTML into commit attrs
window.CommittersProvider = ->

  @importCommits = (html_page) ->
    html_page = $(html_page)
    committers_imgs = html_page.find(".commit img")
    committer_attrs = []

    _.each committers_imgs, (committers_img) ->
      committers_img = $(committers_img)
      committer_attrs.push
        user: committers_img.data("user")
        name: committers_img.attr("alt")
        gravatar: committers_img.attr("src")

    @commits = committer_attrs

  # Parses commits and returns data for Commits model
  @convertCommitsToCommitters = ->
    committers = _.map(_.groupBy(@commits, "user"), (el) ->
      committer = el[0]
      committer.commits = el.length
      committer
    )
    committers

  # Will be used as callback for model Committer's fetch command
  @provider = (html_page) ->
    @importCommits html_page
    @convertCommitsToCommitters()

  return

