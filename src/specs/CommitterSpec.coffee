describe "CommitterSpec", ->
  it "creates an empty Commit", ->
    
    commtter = new Committer
    
    expect(commtter.attributes).toEqual
      user: ""
      name: ""
      gravatar: ""
      commits: 0

    return

  it "creates a Commit", ->
    commtter = new Committer
      user: "999"
      name: "Pierre"
      gravatar: "http://.."
      commits: 33
    
    expect(commtter.attributes).toEqual
      user: "999"
      name: "Pierre"
      gravatar: "http://.."
      commits: 33

    return

  return
