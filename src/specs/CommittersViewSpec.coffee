jasmine.getFixtures().fixturesPath = 'public/static';

describe "App", ->

  beforeEach ->
    loadFixtures 'test-fixture.html'

    @response = readFixtures("commits.html");

    @committers = new CommittersList()
    spyOn(window, 'CommittersList').and.returnValue @committers


  it "Integration test, loads committers into DOM", (done) ->
    
    fakeResponse @response, {}, () ->
      committersView = new CommittersView()

    setTimeout(
      ->
        expect($('#content img.committer').length).toEqual(12);

        _.each $('#content img.committer'), (el, i) ->
          $el = $(el)
          n_commits = parseInt $el.attr('data-commits')
          height = $el.height()

          expect(height).toEqual (n_commits - 1) * 10 + 50

        done()
      ,
      1000
    )


  return
