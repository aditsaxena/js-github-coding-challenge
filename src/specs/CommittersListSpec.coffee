describe "CommittersListSpec", ->
  
  it "creates an empty CommittersList", ->
    committers = new CommittersList()
    expect(committers.models.length).toEqual 0

  it "creates a CommittersList", ->
    
    pierre = new Committer
      user: "999"
      name: "Pierre"
      gravatar: "http://.."
      commits: 33

    jane = new Committer
      user: "555"
      name: "Jane"
      gravatar: "http://.."
      commits: 8
    
    committers = new CommittersList [
      pierre
      jane
    ]

    expect(committers.models.length).toEqual 2
    
  it "creates a CommittersList", (done) ->
    committers = new CommittersList()
    committers.url = "public/static/commits.html"
  
    committers.fetch
      success: ->
        expect(committers.models.length).toEqual 12
        done();

  return
