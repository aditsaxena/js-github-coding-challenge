# Javascript coding challenge

**Please complete this exercise in `test.html` in this folder**

For this test you will need to make a AJAX request to GitHub to get the last 20 commits to rails, and then add the avatars of the committers to the `#content` div. 

The more commits they have made, the bigger the avatar should be. For example:

* 1 commit, 50px x 50px  
* 2 commits, 60px x 60px  
* 3 commits, 70px x 70px  
* ...  

You can use whatever libraries you want, jQuery is already provided in the test.html file. 
Feel free to add any CSS styling you wish as well.

You may also work in Coffeescript if you prefer.

Bonus points for using a JS testing library!

### Example Result:

![Screenshot](https://bytebucket.org/aditsaxena/js-github-coding-challenge/raw/a5e89a5d6bc18bb1e255b2362ee2ef1f7ed6e5ab/example.jpg)